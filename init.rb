Redmine::Plugin.register :redmine_wikiall do
  name 'Wiki all pages'
  author 'Dmitry Klepcha'
  description 'Show all Wiki pages in one.'
  version '0.1.0'
  url 'http://armageddon.org.ru'
  author_url 'http://armageddon.org.ru'
  
  menu :top_menu, :wikiall, {:controller => 'showallpages', :action => 'index', :project_id => nil}, :caption => :my_label, :if => Proc.new { User.current.allowed_to?({:controller => 'showallpages', :action => 'index'}, nil, {:global => true}) }
end
