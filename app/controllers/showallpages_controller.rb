class ShowallpagesController < ApplicationController
  unloadable

  def index
	@pages = WikiPage.all
	@pages_by_parent_id = @pages.group_by(&:parent_id)
  end
end
